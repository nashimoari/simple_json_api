<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use nashimoari\simpleJSONAPI\Controllers\Table;
use nashimoari\simpleJSONAPI\Storage\StorageFactory;


final class MethodTableTest extends TestCase
{

    /**
     *
     */
    public function testEmpty()
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new Table($connection);

        $this->expectExceptionMessage('Missing a required field \'table\'');
        $this->expectException(Exception::class);

        $res = $method->request([]);
    }

    public function testNotExistingTable(): void
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new Table($connection);

        $this->expectExceptionMessage('Access to table \'not_existing_table\' is denied');
        $this->expectException(Exception::class);

        $res = $method->request(['table'=>'not_existing_table']);

    }

    public function testNewsAll(): void
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new Table($connection);

        $res = $method->request(['table'=>'News']);

        $this->assertEquals('select * from News', $res['in']['sql']);
    }

    public function testNewsOnly1(): void {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new Table($connection);

        $res = $method->request(['table'=>'News','id'=>'1']);

        $this->assertEquals('select * from News where id = :id', $res['in']['sql']);
    }
}
