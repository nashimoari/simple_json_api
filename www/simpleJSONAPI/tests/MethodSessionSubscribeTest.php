<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use nashimoari\simpleJSONAPI\Controllers\SessionSubscribe;
use nashimoari\simpleJSONAPI\Storage\StorageFactory;


final class MethodSessionSubscribeTest extends TestCase
{

    /**
     *sessionId - ID сессии, на которую нужно записаться
     * userEmail - email пользователя
     */
    public function testEmpty()
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new SessionSubscribe($connection);

        $this->expectExceptionMessage('Missing a required field: \'sessionId\', \'userEmail\'');
        $this->expectException(Exception::class);

        $res = $method->request([]);
    }


    public function testAbsentSessionId()
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new SessionSubscribe($connection);

        $this->expectExceptionMessage('Missing a required field: \'sessionId\'');
        $this->expectException(Exception::class);

        $res = $method->request(['userEmail'=>'test']);
    }

    public function testAbsentUserEmail()
    {
        $connection = StorageFactory::createConnection('MYSQL_STUB');
        $method = new SessionSubscribe($connection);

        $this->expectExceptionMessage('Missing a required field: \'userEmail\'');
        $this->expectException(Exception::class);

        $res = $method->request(['sessionId'=>'1']);
    }

}
