<?php

namespace nashimoari\simpleJSONAPI\API;

use PHPUnit\Framework\TestCase;

class apiFunctionalTest extends TestCase
{

    /**
     * Prepare database for tests
     */
    public function testDBPrepare() {
        $api = new api_test();
        $api->run('DBDrop',[]);
        $api->run('DBInit',[]);
        $this->assertEquals(1 ,1);

    }

    /**
     * Запрос несуществующего метода
     */
    public function testAbsentMethod()
    {
        $api = new api_test();
        $res = $api->run('absent_method', []);
        $this->assertEquals('Method does not exist', $res['message']);
        $this->assertEquals('error', $res['status']);
    }

    /**
     * Запрос из таблицы с отсутствующими параметром table
     */
    public function testMethodTableAbsentParamTable()
    {
        $api = new api_test();
        $res = $api->run('Table', []);
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('0', count($res['payload']));
        $this->assertEquals('Missing a required field \'table\'', $res['message']);
    }

    /**
     * Запрос из таблицы доступ к которой запрещен (Users)
     */
    public function testMethodTableToStrictTable()
    {
        $api = new api_test();
        $res = $api->run('Table', ['table' => 'Users']);
        $this->assertEquals('0', count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Access to table \'Users\' is denied', $res['message']);
    }

    /**
     * Запрос всех записей из таблицы News
     */
    public function testMethodTableAllRecordsGet()
    {
        $api = new api_test();
        $res = $api->run('Table', ['table' => 'News']);
        $this->assertEquals('ok', $res['status']);
        $this->assertEquals(3, count($res['payload']));
    }

    /**
     * Запрос 1 записи из таблицы News с ID = 1
     */
    public function testMethodTable1RecordsGet()
    {
        $api = new api_test();
        $res = $api->run('Table', ['table' => 'News', 'id' => 1]);
        $this->assertEquals('ok', $res['status']);
        $this->assertEquals(1, count($res['payload']));
    }

    /**
     * Запрос 1 записи из таблицы News с несуществующим ID = 0
     */
    public function testMethodTableAbsentRecordsGet()
    {
        $api = new api_test();
        $res = $api->run('Table', ['table' => 'News', 'id' => 0]);
        $this->assertEquals('ok', $res['status']);
        $this->assertEquals(0, count($res['payload']));
    }

    /**
     * Запрос на запись с отсутствующими параметрами
     */
    public function testMethodSessionSubscribeAbsentParameters()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', []);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Missing a required field: \'sessionId\', \'userEmail\'', $res['message']);
    }

    /**
     * Запрос на запись с отсутствующими параметром userEmail
     */
    public function testMethodSessionSubscribeAbsentParameterUserEmail()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => 1]);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Missing a required field: \'userEmail\'', $res['message']);

    }

    public function testMethodSessionSubscribeAbsentParameterSessionId()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['userEmail' => 'absent@email.ru']);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Missing a required field: \'sessionId\'', $res['message']);

    }

    /**
     * Запрос на запись с несуществующим ящиком
     */
    public function testMethodSessionSubscribeUnknownUser()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => 1, 'userEmail' => 'absent@email.ru']);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Указанный пользователь не зарегистрирован в системе', $res['message']);
    }

    /**
     * Запрос на запись на несуществующую сессию
     */
    public function testMethodSessionSubscribeAbsentSession()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => -1, 'userEmail' => 'user@example.com']);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Session is not exists', $res['message']);

    }

    /**
     * запрос на запись пользователя user@example.com на сессию 1
     * @depends testDBPrepare
     */
    public function testMethodSessionSubscribeAddUserToSession()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => 1, 'userEmail' => 'user@example.com']);
        print_r($res);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('ok', $res['status']);
        $this->assertEquals('Вы успешно записаны на выбранную сессию', $res['message']);
    }

    /**
     * Еще один запрос на запись пользователя user@example.com на сессию 1
     * @depends testMethodSessionSubscribeAddUserToSession
     */
    public function testMethodSessionSubscribeAddUserToSessionAgain()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => 1, 'userEmail' => 'user@example.com']);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('The user already registered at this session', $res['message']);
    }

    /**
     * запрос на запись пользователя user2@example.com на сессию 1 (лимит 1 место)
     * @depends testMethodSessionSubscribeAddUserToSession
     */
    public function testMethodSessionSubscribeAddUser2ToSession()
    {
        $api = new api_test();
        $res = $api->run('SessionSubscribe', ['sessionId' => 1, 'userEmail' => 'user2@example.com']);
        $this->assertEquals(0, count($res['payload']));
        $this->assertEquals('error', $res['status']);
        $this->assertEquals('Извините, все места заняты', $res['message']);
    }

}
