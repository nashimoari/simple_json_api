<?php


namespace nashimoari\simpleJSONAPI\classes;


use ReflectionClass;

class settings
{

    public static function get()
    {

        $test = new settings();
        $reflector = new ReflectionClass($test);
        $appPath = dirname($reflector->getFileName());
        return json_decode(file_get_contents($appPath . '/../Settings.json'), true);
    }

}