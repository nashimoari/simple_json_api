<?php


namespace nashimoari\SimpleJSONAPI\classes;

use nashimoari\SimpleJSONAPI\Storage\IStorage;
class User
{
    private $connection;

    public function __construct(IStorage $connection) {
        $this->connection = $connection;
        return true;
    }

    public function getByEmail($email): Array
    {
        $sql['sql'] = "select * from Users u where u.Email = :email";
        $sql['var']['email'] = $email;

        $res = $this->connection->selectFromTable($sql);

        if (count($res)>1) {
            throw new \Exception('Количество пользователей с указанным почтовым ящиком больше чем 1');
        }

        if (count($res)==1) {
            return $res[0];
        }

        return [];

    }

}