<?php


namespace nashimoari\SimpleJSONAPI\classes;


use nashimoari\SimpleJSONAPI\Storage\IStorage;

class session
{

    private $connection;

    public function __construct(IStorage $connection) {
        $this->connection = $connection;
        return true;
    }

    public function getByID($sessionID) : Array {
        $sql['sql'] = "select * from Session s where s.id = :sessionID";
        $sql['var']['sessionID'] = $sessionID;
        $out = $this->connection->selectFromTable($sql);

        unset($sql);
        $sql['sql'] = "select * from Session s where s.id = :sessionID";
        $sql['var']['sessionID'] = $sessionID;
        $out['participants'] = $this->connection->selectFromTable($sql);

        return $out;
    }
    public function addUserToSession($userID,$sessionID) {
        // start transaction
        $this->connection->transactionBegin();

        // set lock on row
        $sql['sql'] = 'select * from Session where id = :sessionID for update';
        $sql['var']['sessionID'] = $sessionID;
        $sessionInfo = $this->connection->selectFromTable($sql);

        if (count($sessionInfo)<1) {
            throw new \Exception('Session is not exists');
        }

        $sessionInfo = $sessionInfo[0];

        // check that the user is already registered for this session
        if ($this->sessionParticipantCheckExists($userID,$sessionID)) {
            throw new \Exception('The user already registered at this session');
        };

        // count current participants
        unset($sql);
        $sql['sql'] = 'select count(1) cnt from SessionParticipant sp where sp.sessionID = :sessionID';
        $sql['var']['sessionID'] = $sessionID;

        $usersAtSession = $this->connection->selectFromTable($sql)[0];
        if ($usersAtSession['cnt']>=$sessionInfo['ParticipantLimit']) {
            throw new \Exception('Извините, все места заняты');
        }

        // add record to table
        unset($sql);
        $sql['table_name'] = 'SessionParticipant';
        $sql['var'][':sessionID'] = $sessionID;
        $sql['var'][':userID'] = $userID;
        $this->connection->insertToTable($sql);

        // finish transaction
        $this->connection->transaction_end();
    }

    public function sessionParticipantCheckExists($userID,$sessionID) :bool {
        $sql['sql'] = 'select count(1) cnt from SessionParticipant sp where sp.sessionID = :sessionID and sp.userID = :userID';
        $sql['var']['sessionID'] = $sessionID;
        $sql['var']['userID'] = $userID;
        $res = $this->connection->selectFromTable($sql);

        if ($res[0]['cnt']==1) {
            return true;
        }

        if ($res[0]['cnt']>1) {
            throw new \Exception('Количество записей для данного пользователя > 1');
        }

        return false;
    }
}