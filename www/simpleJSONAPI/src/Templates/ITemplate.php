<?php


namespace nashimoari\simpleJSONAPI\Templates;


interface ITemplate
{
  public static function formatAnswer($in) : string;
}