<?php

namespace nashimoari\simpleJSONAPI\API;


interface IAPI
{
  public function run(String $method,Array $data);
}