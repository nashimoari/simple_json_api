<?php


namespace nashimoari\simpleJSONAPI\API;

use Exception;
use nashimoari\simpleJSONAPI\Storage\StorageFactory;

class api_test implements IAPI
{
    public function run(String $method_path, Array $data)
    {
        $out = ['status' => 'ok', 'payload' => []];

        try {

            $connection = StorageFactory::createConnection('MYSQL');
            $connection->instance_set('simpleJSONAPI_local');

            $method_path = 'nashimoari\\simpleJSONAPI\\Controllers\\' . $method_path;

            if (!class_exists($method_path)) {
                throw new Exception('Method does not exist');
            }
            $method = new $method_path($connection);

            $out['payload'] = $method->request($data);
            $out['message'] = $method->messageGet();

        } catch (\Exception $e) {
            $out['status'] = 'error';
            $out['message'] = $e->getMessage();
        }

        return $out;
    }
}