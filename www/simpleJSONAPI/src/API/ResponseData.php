<?php


namespace nashimoari\simpleJSONAPI\Controllers;


class ResponseData
{
    /**
     * Статус операции ("ok" | "error" - ошибка или успех)
     * @var string
     */
    public $status;

    /**
     * Полезная нагрузка (данные) - массив объектов или сам объект
     * @var array
     */
    public $payload;

    /**
     * Опциональный параметр - сообщение, которое нужно отобразить пользователю. Например, "Спасибо, ваша новость сохранена" или "Вы уже опубликовали такую новость".
     * @var string
     */
    public $message;

}