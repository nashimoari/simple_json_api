<?php


namespace nashimoari\simpleJSONAPI\Controllers;


use Exception;
use nashimoari\simpleJSONAPI\classes\session;
use nashimoari\simpleJSONAPI\classes\User;
use nashimoari\simpleJSONAPI\Storage\IStorage;
use ReflectionClass;

class SessionSubscribe implements IController
{
    /**
     * Экземпляр IStorage
     * @var connection
     */
    private $connection;

    /**
     * Настройки метода
     * @var settings
     */
    private $settings;

    private $message="";


    public function __construct(IStorage $connection) {
        $this->connection = $connection;

        /**
         *  подключаем настройки
         */

        $reflector = new ReflectionClass($this);
        $app_path =  dirname($reflector->getFileName());

        $this->settings = json_decode(file_get_contents($app_path.'/../Settings.json'), true)['Methods']['SessionSubscribe'];
        return true;
    }

    public function messageGet(): String
    {
        return $this->message;
    }

    public function request($in): Array
    {
        // check input data
        $absent_fields = '';

        if (!isset($in['sessionId'])) {
            $absent_fields .= '\'sessionId\'';
        }

        if (!isset($in['userEmail'])) {
            if (strlen($absent_fields)>0) {
                $absent_fields .= ', ';
            }
            $absent_fields .= '\'userEmail\'';
        }

        if (strlen($absent_fields)>0) {
            Throw new Exception('Missing a required field: '.$absent_fields);
        }

        // check that user exists
        $user = new User($this->connection);
        $userData = $user->getByEmail($in['userEmail']);

        if (count($userData)==0) {
            Throw new Exception('Указанный пользователь не зарегистрирован в системе');
        }


        $session = new session($this->connection);

        $session->addUserToSession($userData['ID'],$in['sessionId']);

        $this->message = 'Вы успешно записаны на выбранную сессию';

        return [];

    }
}