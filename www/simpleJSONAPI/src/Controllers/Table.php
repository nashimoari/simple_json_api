<?php


namespace nashimoari\simpleJSONAPI\Controllers;

use Exception;
use nashimoari\simpleJSONAPI\Storage\IStorage;
use nashimoari\simpleJSONAPI\API\api;
use ReflectionClass;

class Table implements IController
{

    /**
     * Экземпляр IStorage
     * @var connection
     */
    private $connection;

    /**
     * Настройки метода
     * @var settings
     */
    private $settings;

    private $message="";


    public function __construct(IStorage $connection) {
        $this->connection = $connection;

        /**
         *  подключаем настройки
         */

        $reflector = new ReflectionClass($this);
        $app_path =  dirname($reflector->getFileName());

        $this->settings = json_decode(file_get_contents($app_path.'/../Settings.json'), true)['Methods']['Table'];
        return true;
    }

    public function messageGet(): String
    {
        return $this->message;
    }

    public function request($in): Array
    {
        // check input data
        if (!isset($in['table'])) {
            Throw new Exception('Missing a required field \'table\'');
        }

        // check that the table is available
        if (array_search($in['table'],$this->settings['WhiteList'])===FALSE) {
            Throw new Exception('Access to table \''.$in['table'].'\' is denied');
        }

        // Query formation
        $sql['sql'] = "select * from ".$in['table'];

        if (isset($in['id'])) {
            $sql['var']['id'] = $in['id'];
            $sql['sql'] .= ' where id = :id';
        }

        // Query execution
        return $this->connection->selectFromTable($sql);

        // Return data

    }

}