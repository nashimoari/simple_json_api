<?php


namespace nashimoari\simpleJSONAPI\Controllers;


interface IController
{
  public function request($in):Array;
  public function messageGet():String;
}