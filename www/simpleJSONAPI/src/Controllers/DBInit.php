<?php


namespace nashimoari\simpleJSONAPI\Controllers;


use nashimoari\simpleJSONAPI\Storage\IStorage;
use ReflectionClass;

class DBInit implements IController
{
    private $connection;
    private $message="";

    public function __construct(IStorage $connection)
    {
        $this->connection = $connection;
        return true;
    }

    public function messageGet(): String
    {
        return $this->message;
    }

    public function request($in): Array
    {
        //print_r(scandir('./'));
        $sqlText = file_get_contents('./../database.sql');
        $this->connection->connectToStorageWithOutDBName();
        $this->connection->sqlFileExecute($sqlText);
        return [];
    }


}