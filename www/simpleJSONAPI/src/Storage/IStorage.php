<?php


namespace nashimoari\simpleJSONAPI\Storage;


interface IStorage
{
    public function instance_set($db_instance);

    public function connectToStorage();

    public function insertToTable($in);

    public function selectFromTable($in);
}