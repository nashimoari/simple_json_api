<?php


namespace nashimoari\simpleJSONAPI\Storage;

use Exception;
class StorageFactory
{

    Public static function createConnection($type)
    {
        if ($type == 'MYSQL') {
            return new MYSQLStorage();
        }

        if ($type == 'MYSQL_STUB') {
            return new MYSQLStubStorage();
        }

        throw new Exception('No data provider for this type');
    }

}