<?php


namespace nashimoari\simpleJSONAPI\Storage;

use PDO;
use Exception;
use nashimoari\simpleJSONAPI\classes\settings;

class MYSQLStorage implements IStorage
{
    private $dbh;
    private $flagAutocommit = true;
    protected $connection = [];

    public function instance_set($db_instance)
    {
        $settings = settings::get();
        $this->connection = $settings['DBConnections'][$db_instance];
    }

    public function connectToStorageWithOutDBName()
    {
        $this->dbh = new PDO("mysql:host=" . $this->connection['host'] . ";port=" . $this->connection['port'] . ";charset=utf8", $this->connection['username'], $this->connection['password']);
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //      $this->dbh->query("set names 'utf8'");

        if ($this->flagAutocommit) {
            $r = $this->dbh->query("SET AUTOCOMMIT=1");
            if ($r === false) {
                throw new Exception('Autocommit enable error');
            }
            $this->dbh->beginTransaction();
        } else {
            $r = $this->dbh->query("SET AUTOCOMMIT=0");
            if ($r === false) {
                throw new Exception('Autocommit disable error');
            }
        }
    }

    public function connectToStorage()
    {
        $this->dbh = new PDO("mysql:host=" . $this->connection['host'] . ";port=" . $this->connection['port'] . ";dbname=" . $this->connection['DBName'] . ";charset=utf8", $this->connection['username'], $this->connection['password']);
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //      $this->dbh->query("set names 'utf8'");

        if ($this->flagAutocommit) {
            $r = $this->dbh->query("SET AUTOCOMMIT=1");
            if ($r === false) {
                throw new Exception('Autocommit enable error');
            }

        } else {
            $r = $this->dbh->query("SET AUTOCOMMIT=0");
            if ($r === false) {
                throw new Exception('Autocommit disable error');
            }
        }
    }

    protected function dbhGet()
    {
        if (!isset($this->dbh)) {
            $this->connectToStorage();
        }
        return $this->dbh;
    }

    public function insertToTable($in)
    {
        $dbh = $this->dbhGet();

        // check input values
        if (!isset($in['var'])) {
            throw new Exception('var is absent');
        }

        $rows = '';
        $values = '';
        foreach ($in['var'] as $key => $item) {
            if (strlen($values) > 0) {
                $values .= ', ';
                $rows .= ', ';
            }
            $rows .= substr($key, 1);
            $values .= $key;
            $in['var'][$key] = $this->to_utf($item);
        }
        $values = ' (' . $rows . ') values (' . $values . ')';

        $sql = 'insert into ' . $in['table_name'] . $values;

        $sth = $dbh->prepare($sql);
        $r = $sth->execute($in['var']);
        if ($r === false) {
            error_log(print_r($sth->errorInfo(), 1));
            throw new Exception('Error');
        }

        return $dbh->lastInsertId();
    }

    public function selectFromTable($in)
    {
        $res = [];

        $sth = $this->exec($in);

        if ($sth !== false) {
            while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                $res_tmp[] = $row;
            }
        }

        if (isset($res_tmp)) {
            $res = $res_tmp;
        }
        return $res;
    }

    public function exec($in)
    {
        $dbh = $this->dbhGet();
        if (isset($in['var'])) {
            $sth = $dbh->prepare($in['sql']);
            $r = $sth->execute($in['var']);
            if ($r === false) {
                error_log('error execute sql');
                error_log('input_data:' . print_r($in, 1));
                error_log(print_r($sth->errorInfo(), 1));
                throw new Exception('Error');
            }
        } else {
            $sth = $dbh->query($in['sql']);
        }
        return $sth;
    }

    public function sqlFileExecute($sqlText)
    {
        // Парсим запросы
        $sql_array = explode(";\n", $sqlText);
        for ($i = 0; $i < sizeof($sql_array) - 1; $i++) {
            $sql['sql'] = $sql_array[$i];
            $tmp_res = $this->exec($sql);
        }
        $this->commit();
    }

    public function transactionBegin()
    {
        $this->flagAutocommit = false;
        $dbh = $this->dbhGet();

        try {
            $dbh->beginTransaction();
        } catch (PDOException $e) {
            echo "\r\ncode:" . $e->getCode();
            echo "\r\nmessage:" . $e->getMessage();
        }
    }

    public function commit()
    {
        $dbh = $this->dbhGet();
        $dbh->commit();
    }

    public function rollback()
    {
        $dbh = $this->dbhGet();
        $dbh->rollback();
    }

    public function close()
    {
        $this->dbh = null;
    }

    public function transaction_end()
    {
        $this->commit();
        //$dbh = $this->dbhGet();
        //$dbh->query('set AUTOCOMMIT = on');
        //$dbh->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
        $this->flagAutocommit = true;
    }

    public function inTransaction()
    {
        return $this->dbh->inTransaction();
    }

    protected function to_utf($str)
    {
        if (iconv("UTF-8", "UTF-8", $str) == $str) {
            $is_utf = true;
        } else {
            $is_utf = false;
        }

        if ($is_utf) {
            return $str;
        } else {
            return iconv("cp1251", "UTF-8", $str);
        }
    }

}