<?php


use nashimoari\simpleJSONAPI\API\api;

/**
 *
 */

include_once '../vendor/autoload.php';

$api = new api();

$method = explode('/', $_GET['q'])[2];
$dataJson = file_get_contents('php://input');
if (strlen($dataJson) > 0) {
    $dataArr = json_decode($dataJson, true);
} else {
    $dataArr = [];
}

$apiRes = $api->run($method, $dataArr);

echo json_encode($apiRes);




